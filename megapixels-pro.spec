%define gitcommit 8b3f00945fd9cca8597ed7775f6444608eccdd07
%define relname 20240320git8b3f009

Name:       megapixels-pro
Version:    1.6.1
Release:    0.4.%{relname}%{?dist}
Summary:    GTK4 camera application that knows how to deal with the media request api. Experimental PinePhone Pro support.

License:    GPLv3+
URL:        https://github.com/kgmt0/megapixels
Source0:    https://github.com/kgmt0/megapixels/archive/%{gitcommit}.tar.gz#/megapixels-%{relname}.tar.gz
Source1:	post_install.py

Patch0:		0000-patch-build-gschemas.patch

BuildRequires:  gcc
BuildRequires:  meson
BuildRequires:	cmake

BuildRequires:	pkgconfig(gio-2.0) >= 2.50.0
BuildRequires:	pkgconfig(glib-2.0) >= 2.62.0
BuildRequires:	pkgconfig(gtk4)
BuildRequires:  pkgconfig(libtiff-4)
BuildRequires:	pkgconfig(epoxy)
BuildRequires:  pkgconfig(libfeedback-0.0)
BuildRequires:  pkgconfig(xrandr)
BuildRequires:  inih-devel
BuildRequires:	zbar-devel
BuildRequires:	/usr/bin/xvfb-run
BuildRequires:	/usr/bin/xauth
BuildRequires:	desktop-file-utils
BuildRequires:	libappstream-glib

Requires: hicolor-icon-theme
# for postprocess.sh
Requires: dcraw
Requires: libglvnd-gles

Conflicts: megapixels

%description
A GTK4 camera application that knows how to deal with the media request api. It uses opengl to debayer the raw sensor data for the preview.

%prep
%autosetup -p1 -n megapixels-%{gitcommit}
cp %{SOURCE1} .

%build
%meson
%meson_build

%install
%meson_install


%check
appstream-util validate-relax --nonet %{buildroot}%{_datadir}/metainfo/org.postmarketos.Megapixels.metainfo.xml

desktop-file-validate %{buildroot}/%{_datadir}/applications/org.postmarketos.Megapixels.desktop

LC_ALL=C.UTF-8 xvfb-run sh <<'SH'
%meson_test
SH


%files
%dir %{_datadir}/megapixels
%dir %{_datadir}/megapixels/config

%{_bindir}/megapixels
%{_bindir}/megapixels-camera-test
%{_bindir}/megapixels-list-devices
%{_datadir}/applications/org.postmarketos.Megapixels.desktop
%{_datadir}/icons/hicolor/scalable/apps/org.postmarketos.Megapixels.svg
%{_datadir}/megapixels/config/*
%{_datadir}/megapixels/postprocess.sh
%{_datadir}/metainfo/org.postmarketos.Megapixels.metainfo.xml
%{_datadir}/glib-2.0/schemas/org.postmarketos.Megapixels.gschema.xml

%doc README.md
%license LICENSE


%changelog
* Sat Sep 07 2024 marcin <marcin@ipv8.pl> - 1.6.1-0.4.20240320git8b3f009
- Update to the latest git release

* Sat Sep 07 2024 marcin <marcin@ipv8.pl> - 1.6.1-0.3.20230614git8103e66
- Add missing xrandr BuildRequire dependency

* Sat Sep 07 2024 marcin <marcin@ipv8.pl> - 1.6.1-0.2.20230614git8103e66
- Bump release to trigger rebuild

* Fri Jun 30 2023 marcin <marcin@ipv8.pl> - 1.6.1-0.1.20230614git8103e66
- Upgrade to the latest version of the patchset
- Add a libglvnd-gles dependency

* Sat Feb 4 2023 marcin <marcin@ipv8.pl> - 1.5.2-0.4.20221206gitd5cbd8e
- Upgrade to the latest version of the patchset

* Thu Jan 26 2023 marcin <marcin@ipv8.pl> - 1.5.2-0.3.20221206git3dc718a
- Upgrade to the latest version of the patchset

* Sat Dec 3 2022 marcin <marcin@ipv8.pl> - 1.5.2-0.1.20221127git8721f29
- Upgrade to the latest version of the patchset

* Thu Sep 1 2022 marcin <marcin@ipv8.pl> - 1.5.2-0.1.20220717git8898795
- Branch the repo to support PinePhone Pro

* Thu Jan 20 2022 Fedora Release Engineering <releng@fedoraproject.org> - 1.4.3-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_36_Mass_Rebuild

* Mon Dec 13 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 1.4.3-1
- Update to 1.4.3

* Mon Dec 06 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 1.4.2-1
- Update to 1.4.2

* Fri Nov 12 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 1.4.0-1
- Update to 1.4.0

* Fri Sep 10 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 1.3.0-1
- Update to 1.3.0

* Fri Jul 30 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 1.2.0-1
- Update to 1.2.0

* Thu Jul 22 2021 Fedora Release Engineering <releng@fedoraproject.org> - 1.1.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_35_Mass_Rebuild

* Wed Jun 23 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 1.1.0-1
- Update to 1.1.0

* Tue May 04 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 1.0.1-1
- Update to 1.0.1

* Mon Mar 29 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 0.16.0-1
- Update to 0.16.0

* Sun Feb 21 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 0.15.0-1
- Update to 0.15.0

* Mon Jan 11 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 0.14.0-1
- Update to 0.14.0

* Fri Jan 01 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 0.13.2-1
- Update to 0.13.2

* Tue Dec 15 2020 Torrey Sorensen <torbuntu@fedoraproject.org> - 0.13.1-2
- Adding license and README

* Thu Dec 10 2020 Torrey Sorensen <torbuntu@fedoraproject.org> - 0.13.1-1
- Update to 0.13.1

* Thu Dec 03 2020 Torrey Sorensen <torbuntu@fedoraproject.org> - 0.12.0-2
- Adding dependencies for postprocess.sh

* Sat Nov 14 2020 Torrey Sorensen <torbuntu@fedoraproject.org> - 0.12.0-1
- Initial packaging

